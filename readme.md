# Steps to run react frontend


# install node modules
npm -i


# start the node server
npm start



# My Assumptions

_Score Calculations_
    
-    Based on how fast user is typing a word
    
-   Based upon a multiplier
-   Bonus score upon leveling up
-   Bonus on trending words 

_Word Appearance_
-   Word appearance rate increase with increase in levels
-   When new word appears it mush be pushed to stack
-   Upon correct typing the subsequent word is removed from stack

_Extra Feature Added_
-   User can select the difficulty mode
        i.e [Easy, Medium, Hard]
-   Difficulty Mode values
        i.e 
            Easy - 5 points per word typed correctly and time allotted is 10 s respectively 
            Medium - 10 points per word typed correctly and time allotted is 7 s respectively
            Hard- 15 points per word typed correctly and time allotted is 5 s respectively

_Optional Implementation_
-   Sound Clips
-   Leveling System

_Game Over_
-   User can save its score 
-   Can deny
-   Can play again

_LeaderBoard_
-   Top ten scores
-   Number of times user played
-   Avg Score
